import React from 'react';
import { Text, View } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import Tab1 from '../container/feedTab/index';
import Tab2 from '../container/feedTab/tab2';
import Tab3 from '../container/feedTab/tab3';
import Tab4 from '../container/feedTab/tab4';


const TabNavigator = createMaterialTopTabNavigator(
    {
    Tab1:Tab1,
    Tab2:Tab2,
    Tab3:Tab3,
    Tab4:Tab4,
   
});

export default createAppContainer(TabNavigator);