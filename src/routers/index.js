import React from 'react';
import { createAppContainer,createStackNavigator, createSwitchNavigator } from 'react-navigation';

// Import routes - Importing main scenes directly for now, This should import StackNavigation Components Later
import Login from '../container/login';
import FeedTabs from './tabNavigator'

// Route Configs
const AppNavigator = createStackNavigator(
  {
    Login:Login,
    FeedTabs:FeedTabs
  },
  {
    initialRouteName: 'Login',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      header: null,
    },
  }
);

// Create Tab Navigator

const MainStackNavigation = createAppContainer(AppNavigator);

export default MainStackNavigation;
