import React, { Component } from 'react';
import {Provider} from 'react-redux'
import {Text,Input} from 'native-base'
import App from './container/app';



import configureStore from './store';
const store = configureStore();


export default class Root extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
    };
  }
 
  render() {
    return (
      <Provider store={store}>
        <App />
        </Provider>
    );
  }
}
