/**
 * Logs all actions and states after they are dispatched.
 */
const logger = store => next => (action) => {
    console.log('\n\n---------------------------');
    console.log(`Redux Action Dispatched: ${action.type}`);
    console.log('---------------------------');
    const result = next(action);
    console.log('next state', store.getState());
    console.log(`---------- ${action.type} -----------\n\n`);
    return result;
  };
  
  export default logger;
  