import { createStore, applyMiddleware, compose } from 'redux';

import logger from './middlewares/logging';

import modules from './modules';
import createSagaMiddleware from 'redux-saga'
import rootSaga from './../services';
const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
  const enhancer = compose(applyMiddleware(sagaMiddleware, logger));
  const store = createStore(modules, enhancer);
  sagaMiddleware.run(rootSaga);
  return store;
}
