
import * as types from './../actions/ActionTypes'
export const INITIAL_STATE = {
  email:'',
  password:'',
  login_message : '',
  login_loading: false,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case types.LOGIN_USER:
      return {
        ...state, 
        login_loading: true, 
        login_message: '',
      }
    case types.LOGIN_USER_SUCCESS:
      return {
        ...state, 
        ...INITIAL_STATE, 
        email:action.payload.email,
        password:action.payload.password,
        login_loading:false,
        login_message:action.payload.message
      }
    case types.LOGIN_USER_FAIL:
      return {
        ...state, 
        login_message: action.payload.message,
        login_loading: false,
      }
    default:
      return state
  }
}

