import { combineReducers } from 'redux';

// Reducers
import Login from './login';
import Feed from './feed';
const appReducer = combineReducers({
  Login,
  Feed
});

const rootReducer = (state, action) => {
  const newState = action.type === 'RESET' ? undefined : state;
  return appReducer(newState, action);
};

export default rootReducer;
