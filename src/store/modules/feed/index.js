
import * as types from './../actions/ActionTypes'
export const INITIAL_STATE = {
  data:{},
  feed_message : '',
  feed_loading: false,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case types.FEED_DATA:
      return {
        ...state, 
        feed_loading: true, 
        feed_message: '',
      }
    case types.FEED_DATA_SUCCESS:
      return {
        ...state, 
        ...INITIAL_STATE,
        data:action.payload.data, 
        feed_loading:false,
        feed_message:action.payload.message
      }
    case types.FEED_DATA_FAIL:
      return {
        ...state, 
        feed_message: action.payload.message,
        feed_loading: false,
      }
    default:
      return state
  }
}

