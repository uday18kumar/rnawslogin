import * as types from './ActionTypes';


export const loginUser = (email, password) => {
    return { type: types.LOGIN_USER, payload: { email: email, password: password } }
  }
  
  export const loginUserFail = (message) => {
    return { type: types.LOGIN_USER_FAIL,payload:{message} }
  }
  
  export const loginUserSuccess = (data,message) => {
    return {
        type: types.LOGIN_USER_SUCCESS,
        payload: {data,message}
    }
  }



export const feedData = (data) => {
    return { type:types.FEED_DATA, payload: {data:data} }
    } 
    
    
export const feedDataFail = (message) => {
    return { type:types.FEED_DATA_FAIL, payload: {message} }
    }  


export const feedDataSuccess = (data,message) => {
    return { type:types.FEED_DATA_SUCCESS, payload: {data:data,message:message} }
    }  