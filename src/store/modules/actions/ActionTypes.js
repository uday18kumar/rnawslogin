export const  LOGIN_USER= 'login_user'
export const  LOGIN_USER_SUCCESS= 'login_user_success'
export const  LOGIN_USER_FAIL= 'login_user_fail'

export const FEED_DATA = 'feed_data';
export const FEED_DATA_SUCCESS = 'feed_data_success';
export const FEED_DATA_FAIL = 'feed_data_fail';