import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Icon} from 'native-base'
class Tab4 extends Component {
  static navigationOptions = ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      // if (routeName === 'Home') {
      //   iconName = `ios-information-circle${focused ? '' : '-outline'}`;
      //   // Sometimes we want to add badges to some icons. 
      //   // You can check the implementation below.
      //   IconComponent = HomeIconWithBadge; 
      // } else if (routeName === 'Settings') {
      //   iconName = `ios-options${focused ? '' : '-outline'}`;
      // }

      // You can return any component that you like here!
      return  <Icon name='search' style={{color:tintColor}}/>;
      },
    tabBarOptions:{
      showIcon:true,
      showLabel:false,
      activeTintColor:'#572ef0',
      inactiveTintColor:'#333347',
      style: {
        backgroundColor: '#20202a',
      },
      indicatorStyle: {backgroundColor:'#572ef0'}
    }
});
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems: 'center',}}>
        <Text> Tab 4 </Text>
      </View>
    );
  }
}

export default Tab4;
