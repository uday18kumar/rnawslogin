import React, { Component } from 'react';
import { View, Text , Image ,TouchableOpacity} from 'react-native';
import { Icon,Container } from 'native-base'

import FeedTabNavigator from './tab1Tabs'

class Tab1 extends Component {

    // header component
  static navigationOptions = ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
        // You can return any component that you like here!
          return  <Icon name='list' type={'Entypo'} style={{color:tintColor}}/>;
          },
        tabBarOptions:{
          showIcon:true,
          showLabel:false,
          activeTintColor:'#572ef0',
          inactiveTintColor:'#333347',
          style: {
            backgroundColor: '#20202a',
          },
          indicatorStyle: {backgroundColor:'#572ef0'}
        }
  });

  render() {
    return (
        <Container>
            <FeedTabNavigator />
        </Container>
    );
  }

}

export default Tab1;
