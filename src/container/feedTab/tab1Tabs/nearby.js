import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Icon} from 'native-base'
import {responsiveFontSize,responsiveWidth,responsiveHeight} from 'react-native-responsive-dimensions';


export default class NearBy extends Component {


  // header component
  static navigationOptions = ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
        // You can return any component that you like here!
          return (<View style={{flex:1,width:responsiveWidth(15),
                        alignItems:'center'}}>
          <View>
              <Text style={{
                  color:tintColor,
                  fontWeight: '400',
                  fontSize:responsiveFontSize(1.5)
                  }}>Nearby</Text>  
              <View
                  style={{
                    borderBottomColor: tintColor,
                    borderBottomWidth: responsiveWidth(1),
                    width:responsiveWidth(5),
                    borderRadius:responsiveWidth(1)
                  }}
                />
          </View>      
          </View>);
          },
    tabBarOptions: {
        showIcon:true,
        showLabel:false,
        activeTintColor: '#572ef0',
        inactiveTintColor:'grey',
        style: {
          backgroundColor: 'transparent',
        },
        indicatorStyle: {backgroundColor:'transparent'}
      }
      
  });


  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex: 1,justifyContent:'center',alignItems: 'center',}}>
        <Text> Nearby </Text>
      </View>
    );
  }
}
