import React, { Component } from 'react';

import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';

import Recent from './recent';
import Popular from './popular';
import NearBy from './nearby';
import Trending from './trending';


// Feed Tab inside tabs
const FeedTabNavigator = createMaterialTopTabNavigator(
  {
  Recent:Recent,
  Popular:Popular,
  NearBy:NearBy,
  Trending:Trending,
});

export default createAppContainer(FeedTabNavigator);

