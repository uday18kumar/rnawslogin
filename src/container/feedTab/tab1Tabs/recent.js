import React, { Component } from 'react';
import { View, Text , Image ,TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Icon,Container,Content,Button } from 'native-base'
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import Carousel,{Pagination} from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient' 
import { connect } from 'react-redux';
import * as actions  from '../../../store/modules/actions';



class Recent extends Component {

      // header component
      
  static navigationOptions = ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
        // You can return any component that you like here!
          return (<View style={{flex:1,width:responsiveWidth(15),
                        alignItems:'center'}}>
          <View>
              <Text style={{
                  color:tintColor,
                  fontWeight: '400',
                  fontSize:responsiveFontSize(1.5)
                  }}>Recent</Text>  
              <View
                  style={{
                    borderBottomColor: tintColor,
                    borderBottomWidth: responsiveWidth(1),
                    width:responsiveWidth(5),
                    borderRadius:responsiveWidth(1)
                  }}
                />
          </View>      
          </View>);
          },
    tabBarOptions: {
        showIcon:true,
        showLabel:false,
        activeTintColor: '#572ef0',
        inactiveTintColor:'grey',
        style: {
          backgroundColor: 'transparent',
        },
        indicatorStyle: {backgroundColor:'transparent'}
      }
      
  });

  constructor(props) {
    super(props);
    let interval = null;
    this.state = {
      activeItem:1,
      carousel_data:[]
    };
  }

  componentDidMount(){
    let items = [];
    this.props.feedData();
    this.interval = setTimeout(()=>{
        for(var i=0;i< this.props.data.length ; i++){
          items.push(this.props.data[i]);
        }
      this.setState({carousel_data:items});  
    },500);
    }

    componentWillUnmount(){
      this.setState({carousel_data:[]});
      this.interval = null;
    }

    _renderItem ({item,index}) {

      return (
          <View style={{alignItems:'center',
            paddingTop:responsiveHeight(2)
         }}>
              <Image 
                source={{uri:item.img}}
                style={{width:responsiveWidth(70),
                      height:responsiveHeight(40),
                      borderRadius:responsiveHeight(3),
                      marginVertical:responsiveHeight(-3),
                    
                   }}
                      resizeMode={'contain'}
              />
              <Text style={{textAlign:'center',
                    fontSize:responsiveFontSize(1.5),
                    color:'black'}}>{ item.title }</Text>
          </View>
      );
  }

  render() {
    return (
        <Container>
          <Content contentContainerStyle={{justifyContent:'center',alignItems:'center'}}>
            {/* first div */}
            

            {/* second div */}
            <View style={{justifyContent:'center',width:responsiveWidth(100),alignItems:'center'}}>
                <Carousel
                  ref={(c) => { this._carousel = c; }}
                  data={this.state.carousel_data}
                  renderItem={this._renderItem}
                  sliderWidth={responsiveWidth(100)}
                  itemWidth={responsiveWidth(70)}
                  layout={'default'}
                  firstItem={this.state.activeItem}
                  windowSize={1}
                  onSnapToItem={(index) => this.setState({ activeItem: index }) }
                />

                <TouchableOpacity style={{marginBottom:responsiveHeight(-2)}}>
                    <LinearGradient colors={['#8d43fc','#5956f6']} 
                              style={{alignItems:'center',width:responsiveWidth(35),
                              justifyContent:'center',
                              borderRadius:responsiveWidth(5),
                              height:responsiveHeight(5),
                              marginTop:responsiveHeight(3)}}>
                            <Text style={{textAlign:'center',
                                          fontSize:responsiveFontSize(1.5),
                                          color:'white'}}>
                                  Read full story
                            </Text>
                    </LinearGradient>
                </TouchableOpacity>
                
              
                <Pagination
                  dotsLength={this.state.carousel_data.length}
                  activeDotIndex={this.state.activeItem}
                  dotStyle={{width:responsiveWidth(6),height:responsiveHeight(0.7)}}  
                  dotContainerStyle={{marginHorizontal:responsiveWidth(0)}} 
                  dotColor={'#382481'}
                  inactiveDotColor={'black'}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this._carousel}
                  tappableDots={!!this._carousel}
                />

            </View>

            {/* third tab */}
            <View style={{width:responsiveWidth(80)}}>
              <Text style={{color:'#382481',fontSize:responsiveFontSize(1.5),fontWeight:'bold'}}>
                Top stories for you
              </Text>
              <View style={{alignItems:'center'}}>
                {this.renderImage()}
              </View>  
            </View>
          </Content>
        </Container>
    );
  }

  renderImage(){
    return this.state.carousel_data.map((data) => {
      return (
          <Image 
        source={{uri:data.img}}
        resizeMode={'cover'}
        style={{width:responsiveWidth(90),height:responsiveHeight(35),
                marginTop:responsiveWidth(4),
                borderRadius:responsiveWidth(5)}}
       />
      )
    })
    
  }


}

const mapStateToProps = ({ Feed }) => {
  const { 
      data,
      feed_message, 
      feed_loading,
    } = Feed;

  return { 
    data,
    feed_message, 
    feed_loading,
  }
}

export default connect(mapStateToProps,actions)(Recent)
