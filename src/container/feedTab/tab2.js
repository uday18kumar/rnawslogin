import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {Icon} from 'native-base'



export default class Tab2 extends Component {
  static navigationOptions = ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      return  <Icon name='pencil' type={'FontAwesome'} style={{color:tintColor}}/>;
      },
    tabBarOptions:{
      showIcon:true,
      showLabel:false,
      activeTintColor:'#572ef0',
      inactiveTintColor:'#333347',
      style: {
        backgroundColor: '#20202a',
      },
      indicatorStyle: {backgroundColor:'#572ef0'}
    }
});


  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{flex: 1,justifyContent:'center',alignItems: 'center',}}>
        <Text> Tab 2 </Text>
      </View>
    );
  }
}
