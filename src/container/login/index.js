import React, { Component } from 'react';
import { View,ImageBackground,Image,TouchableOpacity,ToastAndroid,Dimensions} from 'react-native';
import {Container,Content,Text,Icon,Form,Item,Input} from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import {navigationOptions} from 'react-navigation'
import {validateEmail,validatePassword} from '../../utils/validation'
import { connect } from 'react-redux';
import * as actions  from '../../store/modules/actions';

var {height, width} = Dimensions.get('screen');

class Login extends Component {
  static navigationOptions = {
    headerTransparent:true
  };

  constructor(props) {
    super(props);
    this.state = {
      email:'',
      password:''
    };
  }

  componentWillReceiveProps(nextProps){

    if(nextProps.login_message == 'Success')
    {
      this.props.navigation.navigate('FeedTabs');
    }
  }

  onLoginPress(){
    if(this.state.email == '' && this.state.password == '')
    {
      ToastAndroid.show('Please enter all fields ',ToastAndroid.LONG)
      return true
    }
    if(!validateEmail(this.state.email)){
      ToastAndroid.show('Email is not valid',ToastAndroid.LONG)
      return true
    }
    if(!validatePassword(this.state.password)){
      ToastAndroid.show('Password must be greater than 6 characters',ToastAndroid.LONG)
      return true
    }
    else{
      this.props.loginUser(this.state.email,this.state.password);
    }

  }

  render() {
    return (
        <Container>
        
              <ImageBackground source={require('../../../assets/images/loginscreen-layer.png')}
                style={{width:responsiveWidth(100),height:responsiveHeight(98)}}
              >
                <Content>
                  {/* first div */}
                  <View style={{width:responsiveWidth(100),height:responsiveHeight(8),
                                justifyContent:'center',
                            }}>
                      <Text style={{fontSize:responsiveFontSize(2.3),fontStyle:'normal',
                                    fontWeight: 'bold',color:'#382481',
                                    paddingLeft: responsiveWidth(15)}}>
                        LOGIN
                      </Text>
                  </View>

                  {/* second div */}
                  <View style={{paddingTop:responsiveHeight(10),alignItems:'flex-end',justifyContent:'center'}}>
                       <View style={{width:responsiveWidth(70),paddingRight:responsiveWidth(10),paddingVertical:responsiveHeight(5)}}>
                          <Item style={{borderBottomColor:'#c8c8c8',borderBottomWidth:2}}>
                                <Image 
                                source={require('../../../assets/images/user.png')} 
                                resizeMode='contain'
                                style={{width:responsiveWidth(5),
                                        height:responsiveHeight(3)}} />
                            <Input placeholder='Email'
                                    placeholderTextColor='#c8c8c8' 
                                    style={{fontWeight:'bold',
                                            fontSize:responsiveFontSize(1.8)}}
                                    value={this.state.email}  
                                    onChangeText={(text)=>this.setState({email:text})}      
                                    />
                            </Item>
                            <Item style={{borderBottomColor:'#c8c8c8',borderBottomWidth:2}}>
                                <Image 
                                source={require('../../../assets/images/password.png')} 
                                resizeMode='contain' 
                                style={{width:responsiveWidth(5),
                                        height:responsiveHeight(3)}}/>
                            <Input  
                                    secureTextEntry
                                    placeholder='Password'
                                    placeholderTextColor='#c8c8c8' 
                                    style={{fontWeight:'bold',
                                    fontSize:responsiveFontSize(1.8)}}
                                    value={this.state.password}
                                    onChangeText={(text)=>this.setState({password:text})}
                                    />
                            </Item>
                      </View>
                  </View>

                  {/* third div */}
                  <View style={{width:responsiveWidth(100),
                              height:responsiveHeight(40),
                                paddingTop:responsiveHeight(5),
                                alignItems:'center',justifyContent:'center'}}>
                            <TouchableOpacity onPress={()=>this.onLoginPress()}>
                                <Image 
                                  source={require('../../../assets/images/login.png')}
                                  resizeMode={'contain'}
                                  style={{width:responsiveWidth(40),
                                          height:responsiveHeight(40)}}
                                />
                            </TouchableOpacity>
                            
                  </View>
                  </Content>

                  {/* fourth div */}
                  <View style={{
                                rotation:45,
                                flexDirection:'column',
                                bottom:responsiveHeight(5),
                                position:'absolute',
                                left:responsiveWidth(2),
                                flex: 1,
                                justifyContent: 'flex-end',
                                alignSelf:'flex-start',
                               }}>
            
                          <Text style={{
                                      color:'#382481',
                                      fontWeight:'bold',
                                      fontSize:responsiveFontSize(2.3),
                                      alignSelf: 'center',
                                  }}>
                            login via
                          </Text>
                       
                        
                      <View style={{flexDirection:'row',
                                    justifyContent:'center',
                                    paddingTop:responsiveHeight(1)
                                   }}>
                          <View style={{width:responsiveWidth(10)}}>
                                <TouchableOpacity>
                                  <Image source={require('../../../assets/images/social-3.png')} 
                                  resizeMode={'contain'} 
                                  style={{width:responsiveWidth(7),
                                          height:responsiveHeight(7),
                                          rotation:-45}}/>
                                </TouchableOpacity>
                              
                          </View>
                          <View style={{width:responsiveWidth(10)}}>
                                <TouchableOpacity>
                                  {/* <Icon 
                                    name='facebook'
                                    type={'FontAwesome'}
                                    style={{width:responsiveWidth(8),
                                      height:responsiveHeight(8),
                                      paddingTop:responsiveFontSize(2),
                                      color:'white'
                                   }}

                                  /> */}
                                  <Image source={require('../../../assets/images/f.png')} 
                                    resizeMode={'contain'} 
                                    style={{width:responsiveWidth(7),
                                            height:responsiveHeight(7),
                                            rotation:-45}}/>
                                </TouchableOpacity>
                                
                          </View>
                          <View style={{width:responsiveWidth(10)}}>
                                <TouchableOpacity>
                                  <Image source={require('../../../assets/images/social-2.png')} 
                                    resizeMode={'contain'} 
                                    style={{width:responsiveWidth(7),
                                            height:responsiveHeight(7),
                                            rotation:-45}}/>
                                </TouchableOpacity>
                                
                          </View>
                      </View>   
                  </View>
                  {/* fourth div end */}
                 
              </ImageBackground>
            
        </Container>
    );
  }


}

const mapStateToProps = ({ Login }) => {
  const { 
      email,
      password, 
      login_message, 
      login_loading,
      
    } = Login;

  return { 
      email, 
      password,
      login_message, 
      login_loading,
 
  }
}

export default connect(mapStateToProps,actions)(Login)