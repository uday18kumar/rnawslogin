import React, { Component } from 'react';
import { StyleProvider, Root } from 'native-base';

import AppNavigator from './../routers';


class App extends Component {
  render() {
    return (
        <Root>
          <AppNavigator />
        </Root>
    );
  }
}

export default App;
