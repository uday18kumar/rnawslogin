import { fork } from 'redux-saga/effects'
import LoginService from './loginService'
import FeedService from './feedService';
// Just reexport the only saga
export default function* root() {
    yield fork(LoginService)
    yield fork(FeedService)
}