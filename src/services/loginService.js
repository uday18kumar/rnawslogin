import {Platform,AsyncStorage,ToastAndroid} from 'react-native'
import {Toast} from 'native-base'
import { take, call, put, fork, race, takeEvery, takeLatest } from 'redux-saga/effects'
import {delay} from 'redux-saga';
import { NavigationActions } from 'react-navigation'

import * as types from '../store/modules/actions/ActionTypes'
import {
        loginUser,
        loginUserFail,
        loginUserSuccess
} from '../store/modules/actions';

let login_fail = 'Login Failed';
let REQUEST_FAILURE = 'Request Failed';
function* loginFlow() {
    while (true) {
        const loginRequest = yield take(types.LOGIN_USER);

        const auth = {
            email: loginRequest.payload.email,
            password: loginRequest.payload.password
        };
       

        try {
            let response = yield call(loginRequestData,auth);
            let data = {}
            if(response.status) {
              yield put(loginUserSuccess(response.data,response.message));          
            }
            else{
                ToastAndroid.show('Please Check your credentials',ToastAndroid.LONG);
                yield put(loginUserFail(login_fail));
            }
        } catch (e) {
  
            yield put(loginUserFail(REQUEST_FAILURE));
        }
    }
}

async function loginRequestData(auth){
    let email = auth.email.toLowerCase();

    
    let formData = new FormData();
    formData.append("type",'login');
    formData.append("email",auth.email);
    formData.append("password",auth.password);

    return await fetch('http://18.219.194.82/info/service.php', {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                    },
                    body: formData
                })
      .then((response) => response.json())
      .then((responseJson) => {
            return responseJson;
      })
      .catch((error) =>{
        console.error(error);
      })
}



export default function* root() {
    yield fork(loginFlow);
  
}