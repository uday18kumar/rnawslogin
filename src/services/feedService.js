import {Platform,AsyncStorage} from 'react-native'

import { take, call, put, fork, race, takeEvery, takeLatest } from 'redux-saga/effects'
import {delay} from 'redux-saga';
import { NavigationActions } from 'react-navigation'

import * as types from '../store/modules/actions/ActionTypes'
import {
        feedData,
        feedDataFail,
        feedDataSuccess
} from '../store/modules/actions';

let feed_fail = 'Feed Data Not Found';
let REQUEST_FAILURE = 'Request Failed';

function* feedFlow() {
    while (true) {
        try {
            let response = yield call(feedRequestData);
            let data = {}
            if(response.status) {
              yield put(feedDataSuccess(response.data,response.message));          
            }
            else{
                yield put(feedDataFail(feed_fail));
            }
        } catch (e) {
            yield put(feedDataFail(REQUEST_FAILURE));
        }
    }
}

async function feedRequestData(){

    let formData = new FormData();
    formData.append('type','feed');

    return await fetch('http://18.219.194.82/info/service.php', {
                    method: 'POST',
                    headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                },
                    body:formData
                })
      .then((response) => response.json())
      .then((responseJson) => {
          
            return responseJson;
      })
      .catch((error) =>{
        console.error(error);
      })
}



export default function* root() {
    yield fork(feedFlow);
}